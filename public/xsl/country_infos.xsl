<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
    <xsl:param name="country"/>

<xsl:template match="/">
    <xsl:apply-templates select="countries/country[name=$country]"/>
</xsl:template>

<xsl:template match="country">
    <h2>
        <xsl:value-of select="name" />
        &#160;
        <span class="badge badge-light mr-1 align-bottom">
            <xsl:value-of select="translate(name/@cc, $lowercase, $uppercase)" />
        </span>
    </h2>
    <hr />

    <div class="row">
        <div class="col-5 text-center">
            <img src="{flag/@src}" class="img-fluid"/>
        </div>

        <div class="col-7">
            <p> <span class="font-weight-bold"> Capital city: </span>
                <xsl:value-of select="capital"/> </p>
            <p> <span class="font-weight-bold"> Population: </span>
                <xsl:value-of select='format-number(number(population), "#,###,###,###")'/> inhabitants </p>
            <p> <span class="font-weight-bold"> Area: </span>
                <xsl:value-of select='format-number(number(area), "#,###,###,###")'/> km² </p>
        </div>
    </div>

    <hr />

    <h2> Location on Earth </h2>
    <iframe
            width="100%"
            height="400"
            frameborder="0"
            marginheight="0"
            marginwidth="0"
            src="https://maps.google.com/maps?q={$country}&amp;hl=en&amp;output=embed">
    </iframe>


</xsl:template>

</xsl:stylesheet>
