<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="search_name"/>
  <xsl:param name="search_continent"/>

<xsl:template match="/">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Country</th>
        <th>Continent</th>
      </tr>
    </thead>
    <tbody>
      <xsl:for-each select="//country[contains(name, $search_name)
      and contains(@continent, $search_continent)]">
        <xsl:sort select="name"/>
      <tr>
        <td> <img src="{flag/@src}" width="30" heigth="30"/> </td>
        <td> <a class="text-dark" href="/html/country_infos.html?country={name}">
          <xsl:value-of select="name"/> </a> </td>
        <td> <xsl:value-of select="@continent"/> </td>
      </tr>
      </xsl:for-each>
    </tbody>
  </table>
</xsl:template>

</xsl:stylesheet>