#1 Le nom du 1er pays du document
//country[1]/name

#2 La capitale de l'Azerbaijan
//country[name='Azerbaijan']/capital

#3 Le continent du Canada
//country[name='Canada']/@continent

#4 Le nom du pays qui a pour country code (cc): fr
//country/name[@cc='fr']

#5 La population mondiale (somme de la population de tous les pays)
sum(//country/population)

#6 La densité de population (nombre d'habitants au km²) au Japon (Japan)
//country[name='Japan']/population div //country[name='Japan']/area

#7 Le pourcentage de la population mondiale sur le continent asiatique (Asia)
sum(//country[@continent='Asia']/population) div sum(//country/population) * 100

#8 Le nombre de pays qui ont une population supérieure à 1 milliard d'habitants (1e9)
count(//country[population > 1000000000])
