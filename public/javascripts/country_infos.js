async function display() {
    // URL params
    const country = new URL(location).searchParams.get('country');
    //load xml and xsl
    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_infos.xsl');

    //xsl transformations
    const xsltProcessor = new XSLTProcessor();
    xsltProcessor.importStylesheet(xsl);
    xsltProcessor.setParameter(null, 'country', country);
    const result = xsltProcessor.transformToFragment(xml, document);

    //append in #frag_country_infos
    document.querySelector('#frag_country_infos').appendChild(result);
}

display();
