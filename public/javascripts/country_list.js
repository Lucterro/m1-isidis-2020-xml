let search_name = '';
let search_continent = '';

async function display() {
    //load xml and xsl
    const xml = await get('../xml/countries.xml');
    const xsl = await get('../xsl/country_list.xsl');

    //xsl transformations
    const xsltProcessor = new XSLTProcessor();
    xsltProcessor.importStylesheet(xsl);
    xsltProcessor.setParameter(null, 'search_name', search_name);
    xsltProcessor.setParameter(null, 'search_continent', search_continent);
    const result = xsltProcessor.transformToFragment(xml, document);

    // clear and display #frag_country_list
    while (frag_country_list.firstChild) {
        frag_country_list.removeChild(frag_country_list.firstChild);
    }
    //append in #frag_country_list
    frag_country_list.appendChild(result);
}

display();

document.querySelector('#search_name').onkeyup = function() {
    search_name = this.value;
    display();
};

document.querySelector("#search_continent").onchange = function(){
    search_continent = this.value;
    display();
};