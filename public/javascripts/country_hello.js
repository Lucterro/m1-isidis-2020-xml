const API_HELLO_URL = 'https://fourtonfish.com/hellosalut/?cc=';

async function sayHello() {
    // URL params
    const country = new URL(location).searchParams.get('country');
    //load xml
    const xml = await get('../xml/countries.xml');

    let xPathExpr = "//country[name='" + country + "']/name/@cc";
    let xPathResult = xml.evaluate(xPathExpr, xml, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
    let cc = xPathResult.singleNodeValue.textContent;

    //call to API
    const json = await get(API_HELLO_URL+cc, mode='json');
    const res = JSON.parse(json);

    //replace with result from API
    document.querySelector('#say_hello').innerHTML='';
    document.querySelector('#say_hello').innerHTML=res.hello;
}

sayHello();